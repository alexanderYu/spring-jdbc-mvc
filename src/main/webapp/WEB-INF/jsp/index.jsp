<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<html>
<head>

	<title>Home</title>
</head>
<body>

<p>Hello!</p>

<form method="POST" action="add">
		<label> Person </label> <input type='text' name='name'>
		<input type="submit" value="Add Person">
</form>

<br>

<form method="POST" action="search">
	<label> Search Person </label> <input type='text' name='search'>
	<input type="submit" value="Search Pweson">
</form>

<br>

<table border="1">
	<thead>
		<tr>
			<td>ID</td>
			<td>Name</td>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${message}" var="map">
		<tr>
			<c:forEach items="${map}" var="entry">
			<td>${entry.value}</td>
			</c:forEach>
		</tr>
		</c:forEach>
	</tbody>
</table>
</body>
</html>
