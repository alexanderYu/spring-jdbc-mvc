package com.simpletask.services;

import java.util.List;
import java.util.Map;

import com.simpletask.domain.Person;

public interface IPersonService {
	
	void insertPerson(Person person);

	List<Map<String, Object>> getPersonListByName(String name);

	List<Map<String, Object>> selectAllPerson();
	
	
}
