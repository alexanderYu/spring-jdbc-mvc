package com.simpletask.services;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.simpletask.dao.IPersonDAO;
import com.simpletask.domain.Person;

public class PersonServiceImpl implements IPersonService{
	
	@Autowired
	IPersonDAO transformDAO;

	@Override
	public void insertPerson(Person person) {
		transformDAO.insertPerson(person);
	}

	@Override
	public List<Map<String, Object>> getPersonListByName(String name) {
		return transformDAO.getPersonListByName(name);
	}

	@Override
	public List<Map<String, Object>> selectAllPerson() {
		return transformDAO.selectAllPerson();
	}	

}
