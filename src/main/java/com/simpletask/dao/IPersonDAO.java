package com.simpletask.dao;

import java.util.List;
import java.util.Map;

import com.simpletask.domain.Person;

public interface IPersonDAO {
	
	void insertPerson(Person person);

	List<Map<String, Object>> getPersonListByName(String name);

	List<Map<String, Object>> selectAllPerson();	

}
