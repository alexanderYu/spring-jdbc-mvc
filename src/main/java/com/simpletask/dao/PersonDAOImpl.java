package com.simpletask.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.simpletask.domain.Person;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

public class PersonDAOImpl implements IPersonDAO {

	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;

	@Override
	public void insertPerson(Person person) {
		String sql = "INSERT INTO names (name) VALUES (:name)";
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("name", person.getName());
		jdbcTemplate.update(sql, params);
	}

	@Override
	public List<Map<String, Object>> getPersonListByName(String name) {
		String sql = "SELECT * FROM names WHERE UPPER(name) LIKE :name";
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("name", "%" + name.toUpperCase() + "%");
		List<Map<String, Object>> listOfPerson = jdbcTemplate.queryForList(sql, params);
		return listOfPerson;
	}

	@Override

	public List<Map<String, Object>> selectAllPerson() {
		String sql = "SELECT * FROM names";
		MapSqlParameterSource params = new MapSqlParameterSource();
		List<Map<String, Object>> listOfPerson = jdbcTemplate.queryForList(sql, params);
		return listOfPerson;
	}

	private static final class PersonRowMapper implements RowMapper<Person> {

		@Override
		public Person mapRow(ResultSet rs, int rowNum) throws SQLException {
			Person person = new Person();
			person.setName(rs.getString("name"));
			return person;
		}
	}
}
