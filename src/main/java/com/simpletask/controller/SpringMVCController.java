package com.simpletask.controller;

import com.simpletask.domain.Person;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import com.simpletask.services.IPersonService;


@Controller
public class SpringMVCController {
	
	@Autowired 
	IPersonService transformService;
	
	private static final Logger logger = Logger.getLogger(SpringMVCController.class);
	
	@RequestMapping(value = {"/", "added"})
	public String firstPage(ModelMap model) {
		model.addAttribute("message", transformService.selectAllPerson());
		return "index";
	}

	@RequestMapping(value = "/add")
	public String add(ModelMap model, @RequestParam("name") String name) {
		Person person = new Person(name);
		transformService.insertPerson(person);
		model.addAttribute("message", transformService.selectAllPerson());
		return "redirect:/added";
	}

	@RequestMapping(value = "/search")
	public String search(ModelMap model, @RequestParam("search") String name) {
		model.addAttribute("message", transformService.getPersonListByName(name));
		return "index";
	}


}
